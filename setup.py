from setuptools import setup, find_namespace_packages

with open("README.rst", "r") as fh:
    long_description = fh.read()

setup(
    name="fgeom",
    packages=['fgeom'],
    version="0.0.1",
    author="gapag",
    author_email="gp@gapag.st",
    description="finite geometry application",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://bitbucket.org/gapag/4x4square",
    

    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "PyQt5 >= 5.12.1",
    ]

    # # pytest specifics
    # setup_requires=["pytest-runner", ],
    # tests_require=["pytest"],
)
