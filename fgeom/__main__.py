from PyQt5.QtWidgets import QApplication

from fgeom.model.actions import save, export_node, export_above, export_below
from fgeom.view.view import FiniteGeometryEditor

if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    for x in [save, export_node, export_above, export_below]:
        x.setEnabled(False)

    wi = FiniteGeometryEditor()
    wi.show()
    sys.exit(app.exec_())