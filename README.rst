# The 4x4 Square #

This Python-PyQT5 application allows to play with the 4x4 square first 
investigated (to my knowledge) by Steven H. Cullinane.

* Links:
  http://finitegeometry.org/sc/16/geometry.html

### License ###
MIT licensed.
